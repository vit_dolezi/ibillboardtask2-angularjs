'use strict';

angular.module('myApp.tabulka', ['ngRoute',
    'ngMessages'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/tabulka', {
    templateUrl: 'tabulka/tabulka.html',
    controller: 'TabulkaCtrl'
  });
}])

.controller('TabulkaCtrl', function($scope, $mdDialog, $mdMedia) {

    $scope.jobExpireDisable = true;

    $scope.setExpireDateMinDate= function(jobStartDate){

        if(jobStartDate!= null) {
            var jobExpireDateMinDate = new Date(jobStartDate);
            jobExpireDateMinDate.setDate(jobExpireDateMinDate.getDate() + 1);
            $scope.jobExpireDateMinDate = jobExpireDateMinDate;

            $scope.jobExpireDisable = false;
        }else{
            $scope.jobExpireDateMinDate = null;
        }

    };

    $scope.setStartDateMaxDate = function(jobStartDateMaxDate){
        if(jobStartDateMaxDate != null) {
            var jobStartDateMaxDateCopy = new Date(jobStartDateMaxDate);
            jobStartDateMaxDateCopy.setDate(jobStartDateMaxDateCopy.getDate() - 1);
            $scope.jobStartDateMaxDate = jobStartDateMaxDateCopy;
        }else{
            $scope.jobStartDateMaxDate = null;
        }

    };



    $scope.resetAddFormValues = function(){
        $scope.editIndex = -1;
        $scope.newEmployee.id = '';
        $scope.newEmployee.name = '';
        $scope.newEmployee.surname = '';
        $scope.newEmployee.position = positions[0];
        $scope.newEmployee.fixedPeriod = false;
        $scope.newEmployee.birthDay = null;
        $scope.newEmployee.jobStartDate = null;
        $scope.newEmployee.jobExpireDate = null;

        $scope.jobBirthDateMaxDate = dateTimeYesterday;

        $scope.jobStartDateMinDate = dateTimeNow;
        $scope.jobStartDateMaxDate = null;

        $scope.jobExpireDateMinDate = null;
        $scope.jobExpireDisable = true;
    };

    $scope.setOrderKey = function (key) {
        $scope.orderKey = key;
        $scope.orderKeyreverse = !$scope.orderKeyreverse;
        $scope.orderDirection = $scope.orderKeyreverse ? 'DESC' : 'ASC';
    };
    
    $scope.getObjectById = function(id){
        var objects = $scope.tableData.filter(function( obj ) {
            return obj.id == id;
        });
        return objects[0];
    };

    $scope.editItem = function (id) {
        $scope.id = id;
        initFormValues($scope.getObjectById(id), id);
    };

    function initFormValues(personObject, id){
        $scope.editIndex = id;
        $scope.newEmployee.name = personObject.name;
        $scope.newEmployee.surname = personObject.surname;
        $scope.newEmployee.position = personObject.position;
        $scope.newEmployee.fixedPeriod = (personObject.fixedPeriod === true);
        $scope.newEmployee.birthDay = new Date(personObject.birthDay);
        $scope.newEmployee.jobStartDate = new Date(personObject.jobStartDate);
        $scope.newEmployee.jobExpireDate = $scope.fixedPeriod === true ? new Date(personObject.jobExpireDate) : '' ;
    }

    $scope.changeItem = function(id){
        var itemToChange = $scope.getObjectById(id);

        itemToChange.id = $scope.selected.id;
        itemToChange.name = $scope.selected.name;
        itemToChange.surname = $scope.selected.surname;
        itemToChange.position = $scope.selected.position;
        itemToChange.birthDay = $scope.selected.birthDay;
        itemToChange.jobStartDate = ($scope.selected.jobStartDate == 'Invalid Date') ? '' : $scope.selected.jobStartDate;
        itemToChange.jobExpireDate =  $scope.selected.fixedPeriod !==true ? ''  :  $scope.selected.jobExpireDate;
        itemToChange.fixedPeriod = $scope.selected.fixedPeriod;

        $scope.resetSelectedEmployeeObject();
    };

    $scope.pushEmployeeToTableData = function(){
        var employee = {};
        $scope.maxIdValue++;

        employee.id = $scope.maxIdValue;
        employee.name = $scope.newEmployee.name;
        employee.surname = $scope.newEmployee.surname;
        employee.position = $scope.newEmployee.position;
        employee.birthDay = $scope.newEmployee.birthDay;
        employee.jobStartDate = ($scope.newEmployee.jobStartDate == 'Invalid Date') ? '' : $scope.newEmployee.jobStartDate;
        employee.jobExpireDate =  $scope.newEmployee.fixedPeriod !==true ? ''  :  $scope.newEmployee.jobExpireDate;
        employee.fixedPeriod = $scope.newEmployee.fixedPeriod;
        $scope.tableData.push(employee);
    };

    $scope.removeItem = function (id) {
        var len = $scope.tableData.length;
        var indexToDelete;
        for(var i = 0; i < len; i++) {
            if ($scope.tableData[i].id === id) {
                indexToDelete = i;
                break;
            }
        }

        $scope.tableData.splice(indexToDelete, 1);
    };

    $scope.filterByNameSurnamePosition = function(person) {
        if($scope.liveFilter === ''){
          return true;
        }
        return (person.name.indexOf($scope.liveFilter) !== -1)
            || (person.surname.indexOf($scope.liveFilter) !== -1)
            || (person.position.indexOf($scope.liveFilter)!== -1);
    };


    $scope.selected =  {};

    $scope.getTemplate = function (employee) {
        if (employee.id === $scope.selected.id) {
            return 'tabulka/editTableTemplate.html';
        } else{
            return 'tabulka/classicTableTemplate.html';
        }
    };

    $scope.editEmployee = function (employee) {
        $scope.resetSelectedEmployeeObject();
        $scope.selected.id = employee.id;
        $scope.selected.name = employee.name;
        $scope.selected.surname = employee.surname;
        $scope.selected.position = employee.position;
        $scope.selected.birthDay = new Date(employee.birthDay);
        $scope.selected.jobStartDate =  (employee.jobStartDate != null)  ? new Date(employee.jobStartDate) : null;
        $scope.selected.fixedPeriod = JSON.parse(employee.fixedPeriod);
        $scope.selected.jobExpireDate = ($scope.selected.fixedPeriod && employee.jobExpireDate!= null)  ? new Date(employee.jobExpireDate) : null;

        setDateLimitsForEditForm();
    };

    function setDateLimitsForEditForm(){
        $scope.jobBirthDateMaxDate = dateTimeYesterday;
        $scope.jobStartDateMinDate = $scope.selected.birthDay;


        $scope.setExpireDateMinDate($scope.selected.jobStartDate);

        $scope.setStartDateMaxDate($scope.selected.jobExpireDate);

        $scope.jobExpireDisable = !($scope.selected.jobStartDate != null);


    }


    $scope.resetSelectedEmployeeObject = function () {
        $scope.selected = {};
        $scope.jobStartDateMaxDate = null;
        $scope.jobExpireDateMinDate = null;

        $scope.jobExpireDisable = true;
    };

    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

    $scope.showAddNewEmployee = function(ev) {
        $scope.resetAddFormValues();
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'tabulka/addNewEmployeeTemplate.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: useFullScreen,
            scope: $scope,
            preserveScope: true
        });
        
        $scope.$watch(function() {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    function DialogController($scope, $mdDialog) {
        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.addEmployeeFromDialog = function () {
            $scope.pushEmployeeToTableData();
            $mdDialog.hide();
        };
    }

    var positions = ['accountant','programmer','trainee','sys admin','boss'];

    var employeesArr = {"records":[
        {"id":"1","name":"vit","surname":"dolezi","position": "trainee","birthDay": "1995-06-27", "jobStartDate": "2014-01-01","jobExpireDate":"2016-01-01", "fixedPeriod": "true"},
        {"id":"2","name":"test","surname":"test2","position": "boss","birthDay": "1990-06-27", "jobStartDate": "2010-01-01","jobExpireDate": null, "fixedPeriod": false},
        {
            "id":"3",
            "name":"lorem",
            "surname":"ipsum",
            "position": "programmer",
            "birthDay":"1980-01-27",
            "jobStartDate": "2015-01-01",
            "jobExpireDate":"2016-02-01",
            "fixedPeriod": "true"},
        {
            "id":"4",
            "name":"v",
            "surname":"d",
            "position": "programmer",
            "birthDay":"2016-06-10",
            "jobStartDate": "2016-06-22",
            "jobExpireDate":null,
            "fixedPeriod": "true"},
        {
            "id":"5",
            "name":"v",
            "surname":"d",
            "position": "programmer",
            "birthDay":"2016-06-10",
            "jobStartDate": null,
            "jobExpireDate":null,
            "fixedPeriod": "true"}
    ]};

    $scope.newEmployee = {};
    $scope.tableData = employeesArr.records;

    $scope.maxIdValue = $scope.tableData[$scope.tableData.length-1].id;
    $scope.positions = positions;

    $scope.liveFilter = '';

    $scope.orderKey = '';
    $scope.orderKeyreverse = false;

    $scope.editIndex = -1;
    $scope.newEmployee.id = '';
    $scope.newEmployee.name = '';
    $scope.newEmployee.surname = '';
    $scope.newEmployee.position = positions[0];
    $scope.newEmployee.fixedPeriod = false;
    $scope.newEmployee.birthDay = null;
    $scope.newEmployee.jobStartDate = null;
    $scope.newEmployee.jobExpireDate = null;

    var dateTimeNow = new Date();
    var dateTimeYesterday = new Date();
    dateTimeYesterday.setDate(dateTimeNow.getDate() - 1);

    $scope.jobBirthDateMaxDate = dateTimeYesterday;

    $scope.jobStartDateMinDate = dateTimeNow;
    $scope.jobStartDateMaxDate = null;

    $scope.jobExpireDateMinDate = null;
});