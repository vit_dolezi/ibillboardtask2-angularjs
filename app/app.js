'use strict';

// Declare app level module which depends on views, and components
angular.module('ngTask1', [
    'ngRoute',
    'ngMaterial',
    'myApp.tabulka',
    'myApp.version',
    'core'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({redirectTo: '/tabulka'});
}])
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .accentPalette('yellow');
    });;
