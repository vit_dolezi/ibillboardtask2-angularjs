/**
 * Created by vido on 22.6.16.
 */
'use strict';

angular.
module('core').
filter('checkmark', function() {
    return function(input) {
        return input ? '\u2713' : '\u2718';
    };
});